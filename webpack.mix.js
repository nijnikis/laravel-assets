const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/admin_showtimes.css.scss', 'public/css/admin_showtimes.css')
.sass('resources/sass/application.css.scss', 'resources/css/application.css')
.sass('resources/sass/styleguide.css.scss', 'public/css/styleguide.css')
.sass('resources/sass/ticketframe.css.scss', 'public/css/ticketframe.css')
.sass('resources/sass/ticketframe-v2.css.scss', 'public/css/ticketframe-v2.css')
.css(['resources/css/application.css', 'resources/css/royalslider/royalslider.css', 'resources/css/royalslider/skins/default/rs-default.css'], 'public/css/application.css');

